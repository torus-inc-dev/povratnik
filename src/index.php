<?php
   include('session.php');
?>
         <?php include("header.inc"); ?>
            <div class="container-xl">
               <div class="page-header d-print-none">
                  <div class="row g-2 align-items-center">
                     <div class="col">
                        <h2 class="page-title">
                           <span class="fw-normal">Dobrodošao, </span><span><?php echo $login_session; ?></span><span class="fw-normal">!</span>
                        </h2>
                     </div>
                  </div>
               </div>
            </div>
            <div class="page-body">
               <div class="container-xl">
                  <div class="row row-cards">
                     <div class="col-md-6 col-lg-4">
                        <div class="card">
                           <div class="card-status-top bg-yellow-brand"></div>
                           <div class="card-body">
                              <h3 class="card-title fs-1 fw-bold text-center">Nova narudžba</h3>
                              <p class="text-muted fs-4 text-center">Dodajte novu narudžbu.</p>
                           </div>
                           <!-- Card footer -->
                           <div class="card-footer d-flex justify-content-center">
                              <a href="./order.php" class="btn btn-secondary">Kliknite ovdje</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-lg-4">
                        <div class="card">
                           <div class="card-status-top bg-blue"></div>
                           <div class="card-body">
                              <h3 class="card-title fs-1 fw-bold text-center">Pregled narudžbi</h3>
                              <p class="text-muted fs-4 text-center">Pregledajte i/ili zaključite unešene narudžbe</p>
                           </div>
                           <!-- Card footer -->
                           <div class="card-footer d-flex justify-content-center">
                              <a href="./notcompleted-orders" class="btn btn-primary">Kliknite ovdje</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-lg-4">
                        <div class="card">
                           <div class="card-status-top bg-blue"></div>
                           <div class="card-body">
                              <h3 class="card-title fs-1 fw-bold text-center">Izvršene narudžbe</h3>
                              <p class="text-muted fs-4 text-center">Pregled izvršenih narudžbi</p>
                           </div>
                           <!-- Card footer -->
                           <div class="card-footer d-flex justify-content-center">
                              <a href="./completed-orders.php" class="btn btn-primary">Kliknite ovdje</a>
                           </div>
                        </div>
                     </div>            
                  </div>
               </div>
            </div>
      <?php include("footer.inc"); ?>
