<?php
   include('dbcontroller.php');
   $db_handle = new DBController();

   session_start();

   $user_check = $_SESSION['login_user'];
   $query = "SELECT username FROM Users WHERE username = ?";
   $queryUser = $db_handle->selectFrom($query, "s", $user_check);
   $queryUser->bind_result($username);
   while ($queryUser->fetch()) {
       $user["username"] = $username;
   }
   $queryUser->free_result();
   $queryUser->close();

   $login_session = $user["username"];
  
   if(!isset($_SESSION['login_user'])){
      header("location:login.php");
      die();
   }
?>
