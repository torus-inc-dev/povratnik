                <footer class="footer footer-transparent d-print-none">
                    <div class="container-xl">
                        <div class="row text-center flex-row-reverse justify-content-between">
                            <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                                <div class="list-inline-item">
                                    Copyright © 2022.
                                    <a href="https://torus-inc.tech">Torus Inc.</a>
                                    Sva prava pridržana.
                                </div>
                                <div class="list-inline-item">
                                    <a href="./logout.php">Odjavite se</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://unpkg.com/@tabler/core@latest/dist/js/tabler.min.js" defer=""></script>
        <!-- <script src="./dist/js/tabler.min.js" defer=""></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js" integrity="sha512-93wYgwrIFL+b+P3RvYxi/WUFRXXUDSLCT2JQk9zhVGXuS2mHl2axj6d+R6pP+gcU5isMHRj1u0oYE/mWyt/RjA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script> 
            function appendUrl() {
                var array = [];
                var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
                var url = 'remove.php?';

                for (var i = 0; i < checkboxes.length; i++) {
                    array.push(checkboxes[i].value);
                    if(i == 0) {
                        url += 'items%5B%5D=' + checkboxes[i].value;
                    }
                    else {
                        url += '&items%5B%5D=' + checkboxes[i].value;
                    }
                }
                
                window.location.href = url;
                return false;
            }
            </script> 
            <script>
                document.addEventListener("DOMContentLoaded", function() {
                    const list = new List('table-default', {
                        sortClass: 'table-sort',
                        listClass: 'table',
                        valueNames: [ 'sort-name', 'sort-type', 'sort-city', 'sort-score',
                            { attr: 'data-date', name: 'sort-date' },
                            { attr: 'data-progress', name: 'sort-progress' },
                            'sort-quantity',
                        ]
                    });
                })
              </script>
    </body>

</html>