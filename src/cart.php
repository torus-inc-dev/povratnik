<div class="row d-flex justify-content-end">
    <button type="button" href="#" class="btn btn-primary position-relative btn-icon btn-icon-truck icon-tabler" aria-label="Button" data-bs-toggle="modal" data-bs-target="#modal-report">
        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-truck-delivery" width="24%" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
        <desc>Download more icon variants from https://tabler-icons.io/i/truck-delivery</desc>
        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
        <circle cx="7" cy="17" r="2"></circle>
        <circle cx="17" cy="17" r="2"></circle>
        <path d="M5 17h-2v-4m-1 -8h11v12m-4 0h6m4 0h2v-6h-8m0 -5h5l3 5"></path>
        <line x1="3" y1="9" x2="7" y2="9"></line>
        </svg>
        <?php
        if(!empty($_SESSION["cart_item"])):
        ?>
        <span class="position-absolute h-3 w-3 top-0 start-75 translate-middle badge badge-pill bg-yellow-brand text-primary text-center p-0"><?php echo count($_SESSION["cart_item"]); ?></span>
        <?php
        endif;
        ?>
    </button>
</div>

    
<div id="modal-report" class="modal modal-blur fade" tabindex="-1" role="dialog" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-status bg-blue-brand"></div>
            <div class="modal-header">
            <h5 class="modal-title">Stavke narudžbe</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="table-responsive-sm table-responsive-md">
                    <table class="table table-vcenter card-table">
                        <thead>
                            <tr>
                                <th class="w-0"></th>
                                <th class="text-center">Rb.</th>
                                <th>Pakiranje</th>
                                <th>Dužina</th>
                                <th>Veličina</th>
                                <th>Količina</th>
                                <!-- <th class="w-5"></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if(!empty($_SESSION["cart_item"])):
                            $productId;
                            $sequence = 1;
                            foreach ($_SESSION["cart_item"] as $item):
                                $productId = $item["product_id"];
                                $_SESSION["cart_item"][$productId]["item_no"] = $sequence;
                            ?>
                            <tr class="bg-white">
                                <td>
                                    <div class="d-flex py-1 align-items-center">
                                        <div class="flex-fill">
                                            <div class="font-weight-medium text-center">
                                            <label class="form-check">
                                                <input name="items[]" class="form-check-input" type="checkbox" value="<?php echo $item["product_id"];?>">                          
                                            </label>  
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td data-label="Redni broj">
                                    <div class="d-flex py-1 align-items-center">
                                        <div class="flex-fill">
                                            <div class="font-weight-medium text-center"><?php echo $_SESSION["cart_item"][$productId]["item_no"].'.'; $sequence++; ?></div>
                                        </div>
                                    </div>
                                </td>
                                <td data-label="Pakiranje">
                                    <div class="d-flex py-1 align-items-center">
                                        <div class="flex-fill">
                                            <div class="form-selectgroup-label-content d-flex align-items-center">
                                                <img class="avatar" src="<?php echo $item["package-image"] ?>"  width="125" height="120" />
                                                <div class="font-weight-medium"><?php echo $item["package_type"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td data-label="Dužina">
                                    <div class="d-flex py-1 align-items-center">
                                        <div class="flex-fill">
                                            <div class="form-selectgroup-label-content d-flex align-items-center">
                                                <img class="avatar" src="<?php echo $item["log-length-image"] ?>"  width="125" height="120" />
                                                  <div class="font-weight-medium"><?php echo $item["length"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td data-label="Veličina">
                                    <div class="d-flex py-1 align-items-center">
                                        <div class="flex-fill">
                                            <div class="font-weight-medium"><?php echo $item["size"]; ?></div>
                                        </div>
                                    </div>
                                </td>
                                <td data-label="Količina">
                                    <div class="d-flex py-1 align-items-center">
                                        <div class="flex-fill">
                                            <div class="font-weight-medium"><?php echo $item["quantity"]; ?></div>
                                        </div>
                                    </div>
                                </td>
                                <!-- <td>
                                    <div class="btn-list flex-nowrap">
                                        <a class="btn" href="#">
                                            Uredite
                                        </a>
                                    </div>
                                </td> -->
                            </tr>
                            <?php
                            endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>           
            </div>
            
            <div class="modal-footer border-top py-1">
                <a id="remove" onClick="return appendUrl()" href="" class="btn btn-outline-danger">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <!-- SVG icon code -->
                    Obrišite odabrane stavke
                </a>
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <!-- SVG icon code -->
                    <a href="order.php" class="btn btn-secondary">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <!-- SVG icon code -->
                    Dodajte novu stavku
                </a>
                <?php if(!empty($_SESSION["cart_item"])):?>
                <a href="customer.php" class="btn btn-primary ms-auto">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <!-- SVG icon code -->
                    Unesite kupca
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>