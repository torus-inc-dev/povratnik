<?php

require_once("credentials.php");

class DBController
{
    private $credentials;
    private $conn;
    private $insertId;


    public function __construct()
    {
        $this->credentials = new Credentials();
        $this->conn = $this->connectDB();
        $this->conn->set_charset("utf8mb4");
    }

    public function connectDB()
    {
        $conn = mysqli_connect($this->credentials->getHost(), $this->credentials->getUser(), $this->credentials->getPassword(), $this->credentials->getDatabase());
        return $conn;
    }

    public function setInsertId()
    {
        $this->insertId = $this->conn->insert_id;
    }

    public function getInsertId()
    {
        return $this->insertId;
    }

    public function runQuery($query)
    {
        $result = mysqli_query($this->conn, $query);
        while ($row=mysqli_fetch_assoc($result)) {
            $resultset[] = $row;
        }
        if (!empty($resultset)) {
            return $resultset;
        }
    }

    public function numRows($query)
    {
        $result  = mysqli_query($this->conn, $query);
        $rowcount = mysqli_num_rows($result);
        return $rowcount;
    }

    public function insertInto($query)
    {
      $result = $this->conn->prepare($query);
      return $result;
    }

    public function selectFrom($query)
    {
      $args = func_get_args();
      $args = array_diff($args, [$query]);
      $args = array_values($args);
      $args = $this->makeValuesReferenced($args);

      $result = $this->conn->prepare($query) or die($this->conn->error);
      call_user_func_array(array($result, 'bind_param'), $args);
      $result->execute();
      if(!strpos($query,'*')) $result->store_result();
      return $result;
    }

    public function makeValuesReferenced(&$arr)
    {
      $refs = array();
      foreach($arr as $key => $value) $refs[$key] = &$arr[$key];
      return $refs;
    }
}
