<?php
include('session.php');
require_once("dbcontroller.php");
$db_handle = new DBController();

$package = $db_handle->runQuery("SELECT * FROM Package ORDER BY package_id ASC");
$log_length = $db_handle->runQuery("SELECT * FROM Log_length ORDER BY log_length_id ASC");
$log_size = $db_handle->runQuery("SELECT * FROM Log_size ORDER BY log_size_id ASC");

include("header.inc");
?>
<div class="container-xl">
  <div class="page-header d-print-none">
    <div class="row g-2 align-items-center">
      <div class="col">
        <h2 class="page-title">
          <span>Dodajte stavku narudžbe:</span>
        </h2>
      </div>
    </div>
  </div>
</div>
<div class="page-body">
  <div class="container-xl">
    <form method="post" action="add.php">
      <div class="row row-deck">
        
        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
          <div class="card">
            <div class="card-status-top bg-blue-brand"></div>
            <div class="card-body">
              <label class="form-label fs-3 fw-bold text-center">1. Odaberite pakiranje</label>
              <div class="form-selectgroup form-selectgroup-boxes d-flex">

                <?php 
                if (!empty($package)):
                foreach ($package as $key=>$value):
                ?>

                <label class="form-selectgroup-item flex-fill">
                  <input class="form-selectgroup-input" type="radio"  name="package-id" value="<?php echo $package[$key]["package_id"] ?>" required>
                  <div class="form-selectgroup-label p-3">
                    <div class="d-flex justify-content-center">
                      <img src="<?php echo $package[$key]["image"] ?>"  width="125" height="120" />
                    </div>
                    <div class="d-flex justify-content-center mt-1">
                      <div class="me-1">
                        <span class="form-selectgroup-check"></span>
                      </div>
                      <div>
                        <span><?php echo $package[$key]["type"] ?></span>                      
                      </div>
                    </div>
                  </div>
                </label>

                <?php 
                endforeach;
                endif; 
                ?>

              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
          <div class="card">
            <div class="card-status-top bg-blue-brand"></div>
            <div class="card-body">
              <label class="form-label fs-3 fw-bold text-center">2. Odaberite dužinu</label>
              <div class="form-selectgroup form-selectgroup-boxes d-flex">

                <?php 
                if (!empty($log_length)):
                  foreach ($log_length as $key=>$value):
                ?>

                <label class="form-selectgroup-item flex-fill">
                  <input class="form-selectgroup-input" type="radio"  name="log-length-id" value="<?php echo $log_length[$key]["log_length_id"] ?>" required>
                  <div class="form-selectgroup-label p-3">
                    <div class="d-flex justify-content-center">
                      <img src="<?php echo $log_length[$key]["image"] ?>"  width="125" height="120" class="" />
                    </div>
                    <div class="d-flex justify-content-center mt-1">
                      <div class="me-1">
                      <span class="form-selectgroup-check"></span>
                      </div>
                      <div>
                      <span><?php echo $log_length[$key]["length"] ?></span>
                      
                      </div>
                    </div>
                  </div>
                </label>

                <?php 
                  endforeach;
                endif; 
                ?>

              </div>
            </div>
          </div>
        </div>
      
        
        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
          <div class="card">
            <div class="card-status-top bg-blue-brand"></div>
            <div class="card-body">
              <h3 class="card-title fs-3 fw-bold text-center">3. Odaberite veličinu</h3>

              <div class="form-selectgroup form-selectgroup-boxes d-flex">

                <?php 
                if (!empty($log_size)):
                  foreach ($log_size as $key=>$value):
                ?>

                <label class="form-selectgroup-item flex-fill">
                  <input class="form-selectgroup-input" type="radio"  name="log-size-id" value="<?php echo $log_size[$key]["log_size_id"] ?>" required>
                  <div class="form-selectgroup-label">
                    <div class="d-flex justify-content-center">
                      <img src="<?php //echo $log_length[$key]["image"] ?>"  width="81" height="81" class="" />
                    </div>
                    <div class="d-flex justify-content-center mt-1">
                      <div class="me-1">
                      <span class="form-selectgroup-check"></span>
                      </div>
                      <div>
                      <span><?php echo $log_size[$key]["size"] ?></span>                      
                      </div>
                    </div>
                  </div>
                </label>

                <?php 
                  endforeach;
                endif; 
                ?>

              </div>
            </div>
          </div>
        </div>
        
        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
          <div class="card">
            <div class="card-status-top bg-blue-brand"></div>
            <div class="card-body px-0">
              <h3 class="card-title fs-3 fw-bold text-center">4. Odaberite količinu</h3>
              <div class="d-flex justify-content-center align-items-baseline">
                <input class="form-control w-5 fs-1 text-center ps-0" type="number" name="quantity" value="1" maxlength="2" size="2" />
              </div>
              <p class="text-muted fs-5 text-center mt-2">BROJ PALETA/VREĆA</p>
              <div class="card-footer d-flex justify-content-center">
                <input name="add-to-cart" type="submit" class="btn btn-primary" value="Kliknite ovdje"/>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>

    <?php
      if(isset($_POST["add-to-cart"])) {
       ?> <div class="toast show" role="alert" aria-live="assertive" aria-atomic="true" data-bs-autohide="false" data-bs-toggle="toast">
  <div class="toast-header">
    <span class="avatar avatar-xs me-2" style="background-image: url(...)"></span>
    <strong class="me-auto">Mallory Hulme</strong>
    <small>11 mins ago</small>
    <button type="button" class="ms-2 btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
  </div>
  <div class="toast-body">
    Hello, world! This is a toast message.
  </div>
</div>
      <?php }


    ?>

    <?php include("cart.php"); ?>

  </div>
</div>
<?php include("footer.inc");?>
