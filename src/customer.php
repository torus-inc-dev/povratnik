<?php
  include('session.php');
  require_once("dbcontroller.php");
  $db_handle = new DBController();

  $street = $db_handle->runQuery("SELECT * FROM Street ORDER BY name ASC");
  $city = $db_handle->runQuery("SELECT * FROM City ORDER BY name ASC");
  $orderId = $db_handle->runQuery("SELECT order_id FROM Orders ORDER BY order_id DESC LIMIT 1");

  include("header.inc");
  
?>
<div class="container-xl">
  <div class="page-header d-print-none">
    <div class="row g-2 align-items-center">
      <div class="col">
        <h2 class="page-title">
          <span>Dodajte informacije o narudžbi:</span>
        </h2>
      </div>
    </div>
  </div>
</div>
<div class="page-body">
  <div class="container-xl">
    <div class="row">

      <form method="post" action="new-order.php">
        <div class="col-md-7 col-lg-7 col-xl-7 mx-auto">
          <div class="card">
            <div class="card-status-top bg-blue-brand"></div>
            <div class="card-body">
              <div class="row d-flex justify-content-evenly flex-wrap">

                <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                  <label class="form-label">Ulica</label>
                  <input class="form-control" list="streetList" name="street" required>
                  <datalist id="streetList">
                  <?php
                  if (!empty($street)):
                    foreach ($street as $key=>$value): ?>
                    <option value="<?php echo $street[$key]["name"]; ?>"></option>
                  <?php
                    endforeach;
                  endif;
                  ?>
                  </datalist>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                  <label class="form-label">Grad</label>
                  <input class="form-control" list="cityList" name="city" required>
                  <datalist id="cityList">
                  <?php
                  if (!empty($city)):
                    foreach ($city as $key=>$value): ?>
                    <option value="<?php echo $city[$key]["name"]; ?>"></option>
                  <?php
                    endforeach;
                  endif;
                  ?>
                  </datalist>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                  <label class="form-label">Broj telefona</label>
                  <input class="form-control" type="tel" pattern="([0-9]+.{0,1}[0-9]*,{0,1})*[0-9]" name="phone"  autocomplete="off" required>
                </div>
                <div class="input-icon col-sm-12 col-md-6 col-lg-6 mt-3">
                  <label class="form-label">Isporuka(Mjesec)</label>
                  <input id="datepicker-icon" class="form-control" type="month" name="delivery-date">
                </div>
                <div class="">
                  <label class="form-label">Napomena</label>
                  <textarea class="form-control border-start-textarea" name="note" id="" cols="30" rows="5"></textarea>
                </div>
              </div>
            </div>
            <div class="card-footer d-flex justify-content-between">
              <a href="" class="btn btn-light" data-bs-toggle="modal" data-bs-target="#modal-report">Provjerite stavke</a>
              <!-- <input type="submit" name="submit" href="new-order.php" class="btn btn-secondary" value="Potvrdite informacije"> -->
              <button type="button" href="#" class="btn btn-secondary" aria-label="Button" data-bs-toggle="modal" data-bs-target="#modal-customer">
                Potvrdite informacije >
              </button>
              
            </div>
          </div>
        </div>
        <div id="modal-customer" class="modal modal-blur fade" tabindex="-1" role="dialog" aria-modal="true" role="dialog">
          <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-status bg-blue-brand"></div>
              <div class="modal-body">
                <div class="modal-title">
                  Jeste li sigurni da želite potvrditi narudžbu?
                </div>
                <div>
                  Pritiskom na gumb unosite narudžbu u bazu.
                </div>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-link text-dark text-decoration-underline me-auto" data-bs-dismiss="modal">
                  Odustanite
                </a>
                <input type="submit" name="submit" href="new-order.php" class="btn btn-secondary" value="Potvrđujem narudžbu">
              </div>
            </div>
          </div>
        </div>
  
      </form>
    </div>


    <?php include("cart.php"); ?>

  </div>
</div>
<?php include("footer.inc"); ?>