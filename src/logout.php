<?php
   session_start();
   unset($_SESSION['login_user']);
   session_destroy();
   header("Refresh:0;" . "login.php", true, 302);
?>
