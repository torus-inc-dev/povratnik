<?php
require_once("dbcontroller.php");
$db_handle = new DBController();

if(isset($_POST['update'])) {

    for ($i = 0; $i < count($_POST['item_id']); $i++) {
        echo "The number is:" . $_POST['item_id'][$i] . "<br>";
        $query = "INSERT INTO Order_items(order_id, item_id, package_id, log_length_id, log_size_id, quantity) VALUES (NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,'')) ON DUPLICATE KEY UPDATE package_id = ?, log_length_id = ?, log_size_id = ?, quantity = ?";
        
        $insertOrderItems = $db_handle->selectFrom($query, "iiiiiiiiii", $_POST['order_id'], $_POST['item_id'][$i], $_POST['package-id'][$i], $_POST['log-length-id'][$i], $_POST['log-size-id'][$i], $_POST['quantity'][$i], $_POST['package-id'][$i], $_POST['log-length-id'][$i], $_POST['log-size-id'][$i], $_POST['quantity'][$i]);
        
        $insertOrderItems->close();
    }

    $query = "UPDATE Orders SET note = ?, delivery_date = ? WHERE order_id = ?";
    $update = $db_handle->selectFrom($query, "ssi", $_POST["note"], date('Y-m-d',strtotime($_POST["delivery-date"])), $_POST['order_id']);
    $update->close();
}

if(isset($_POST['delete'])) {

    foreach ($_POST['items'] as $item) {
        $query = "DELETE FROM Order_items WHERE order_id = ? AND item_id = ?";
        $deleteOrderItems = $db_handle->selectFrom($query, "ii", $_POST['order_id'], $item);
        $deleteOrderItems->close();
    }

}

if(isset($_POST['add'])) {
    $query = "INSERT INTO Order_items(order_id, item_id, package_id, log_length_id, log_size_id, quantity) VALUES (NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,''))";
    
    $addOrderItem = $db_handle->selectFrom($query, "iiiiii", $_POST['order_id'], end($_POST['item_id']) + 1, 1, 1, 1, 1);
    
    $addOrderItem->close();
}

header("Refresh:0;" . "update.php?order_id=" . $_POST['order_id'], true, 302);

?>