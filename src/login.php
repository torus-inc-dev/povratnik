<?php
   include("dbcontroller.php");
   $db_handle = new DBController();

   session_start();

   if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["username"])) {
      // username and password sent from form
      $query = "SELECT user_id FROM Users WHERE username = ? AND password = ?";
      $queryUser = $db_handle->selectFrom($query, "ss", $_POST["username"], md5($_POST["password"]));
      $queryUser->bind_result($userId);
      while ($queryUser->fetch()) {
          $user["id"] = $userId;
      }
      $queryUser->free_result();
      $queryUser->close();

      if(!empty($user))
      {
         $count = count($user);
         // If result matched $myusername and $mypassword, table row must be 1 row
   
         if($count == 1) {
            $_SESSION['login_user'] = $_POST["username"];
   
            header("location: index.php");
         }else {
            $error = "Pogrešan unos korisničkog imena ili lozinke.";
         }
      }

   }
?>
<!DOCTYPE html>
<html>

   <head>
      <title>Narudžba drva</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
      <link href="https://unpkg.com/@tabler/core@latest/dist/css/tabler.min.css" rel="stylesheet">
      <!-- <link href="./dist/css/tabler.min.css" rel="stylesheet"> -->
      <link href="style.css" type="text/css" rel="stylesheet" /> 
   </head>
   <body class="border-top-wider border-primary d-flex flex-column">
      <div class="page page-center">
         <div class="container-tight py-4">
            <div class="text-center mb-4">
               <a class="navbar-brand navbar-brand-autodark" href=".">
                  <img src="logo.svg" alt="" height="36">
               </a>
            </div>
            <form class="card card-md" action="" method="post">
               <div class="card-body">
                  <h2 class="card-title text-center mb-4">Prijavite se</h2>
                  <div class="mb-3">
                     <label class="form-label">Korisničko ime</label>
                     <input class="form-control" type="text" name="username" placeholder="Korisničko ime" autocomplete="off">
                  </div>
                  <div class="mb-3">
                     <label class="form-label">Lozinka</label>
                     <div class="input-group input-group-flat">
                        <input class="form-control" type="password" name="password" placeholder="Lozinka" autocomplete="off">
                     </div>
                  </div>
                  <div class="mb-2">
                     <input class="btn btn-primary w-100" type="submit" value="Prijavite se">
                  </div>
               </div>
            </form>
            <div class="mt-3 text-center text-danger"><?php if(isset($_POST["submit"])) echo $error; ?></div>
         </div>
      </div>      
   </body>
</html>
