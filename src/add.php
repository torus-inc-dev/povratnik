<?php
require_once("dbcontroller.php");
$db_handle = new DBController();

session_start();


if (!empty($_POST["quantity"]) && !empty($_POST["log-length-id"]) && !empty($_POST["log-size-id"])) {

    $query = "SELECT * FROM Package WHERE package_id= ?";
    $stmt = $db_handle->selectFrom($query, "i", $_POST["package-id"]);
    $queryPackage = $stmt->get_result();
    while ($row = $queryPackage->fetch_assoc()) {
        $package["id"] = $row['package_id'];
        $package["type"] = $row['type'];
        $package["image"] = $row['image'];
    }
    $stmt->free_result();
    $stmt->close();

    $query = "SELECT * FROM Log_length WHERE log_length_id = ?";
    $stmt = $db_handle->selectFrom($query, "i", $_POST["log-length-id"]);
    $queryLogLength = $stmt->get_result();
    while ($row = $queryLogLength->fetch_assoc()) {
        $logLength["id"] = $row['log_length_id'];
        $logLength["length"] = $row['length'];
        $logLength["image"] = $row['image'];
    }
    $stmt->free_result();
    $stmt->close();

    $query = "SELECT * FROM Log_size WHERE log_size_id= ?";
    $stmt = $db_handle->selectFrom($query, "i", $_POST["log-size-id"]);
    $queryLogSize = $stmt->get_result();
    while ($row = $queryLogSize->fetch_assoc()) {
        $logSize["id"] = $row['log_size_id'];
        $logSize["size"] = $row['size'];
    }
    $stmt->free_result();
    $stmt->close();

    $product = $_POST["package-id"].$_POST["log-length-id"].$_POST["log-size-id"];
    $productArray = array($product=>array('product_id'=>$product, 'item_no'=>'', 'package_id' => (int)$package["id"], 'package_type' => $package["type"], 'log_length_id'=>(int)$logLength["id"],  'length'=>$logLength["length"], 'log_size_id'=>(int)$logSize["id"], 'size'=>$logSize["size"], 'quantity'=>$_POST["quantity"], 'log-length-image'=>$logLength["image"], 'package-image'=>$package["image"]));

    if (!empty($_SESSION["cart_item"])) {
        if (in_array($product, array_keys($_SESSION["cart_item"]))) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ($product == $k) {
                    if (empty($_SESSION["cart_item"][$k]["quantity"])) {
                        $_SESSION["cart_item"][$k]["quantity"] = 0;
                    }
                    $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                }
            }
        } else {
            $_SESSION["cart_item"] += $productArray;
        }
    } else {
        $_SESSION["cart_item"] = $productArray;
    }
}
header("Refresh:0;" . "order.php", true, 302);

?>