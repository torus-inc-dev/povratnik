<?php
include('session.php');

require_once("dbcontroller.php");

$db_handle = new DBController();
$count = 0;

$orders = $db_handle->runQuery("SELECT * FROM Orders WHERE order_status_id = 1");

include("header.inc");

?>

<div class="container-xl">
  <div class="page-header d-print-none">
    <div class="row g-2 align-items-center">
      <div class="col">
        <h2 class="page-title">
          <span>Pregled narudžbi</span>
        </h2>
      </div>
    </div>
  </div>
</div>
<div class="page-body">
  <div class="container-xl">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive-sm" id="table-default">
          <table class="table table-nowrap accordion" id="accordion-example">
            <thead>
              <tr>
                <th>Narudžba br.</th>
                <th>Grad</th>
                <th>Ulica</th>
                <th>Broj telefona</th>
                <th><button class="table-sort" data-sort="sort-date">Isporuka</button></th>
              </tr>
            </thead>
              <?php
              if(!is_null($orders)) :
                foreach ($orders as $order) :
                  $count++;
                
                  $query = "SELECT Street.name FROM Orders INNER JOIN Street ON Orders.street_id = Street.street_id WHERE order_id = ?";
                  $queryStreet = $db_handle->selectFrom($query, "i", $order["order_id"]);
                  $queryStreet->bind_result($street_name);
                
                  while($queryStreet->fetch()) {
                    $street["name"] = $street_name;
                  }
                  $queryStreet->free_result();
                  $queryStreet->close();
                
                  $query = "SELECT City.name FROM Orders INNER JOIN City ON Orders.city_id = City.city_id WHERE order_id = ?";
                  $queryCity = $db_handle->selectFrom($query, "i", $order["order_id"]);
                  $queryCity->bind_result($city_name);
                
                  while($queryCity->fetch()) {
                    $city["name"] = $city_name;
                  }
                  $queryCity->free_result();
                  $queryCity->close();
                
                  $query = "SELECT Order_status.order_status_description FROM Orders INNER JOIN Order_status ON Orders.order_status_id = Order_status.order_status_id WHERE order_id = ?";
                  $queryOrderStatus = $db_handle->selectFrom($query, "i", $order["order_id"]);
                  $queryOrderStatus->bind_result($description);
                
                  while($queryOrderStatus->fetch()) {
                    $order_status["description"] = $description;
                  }
                  $queryOrderStatus->free_result();
                  $queryOrderStatus->close();
                
                  $query = "SELECT quantity, Package.type, Log_length.length, Log_length.image, Log_size.size FROM Order_items INNER JOIN Package ON Order_items.package_id = Package.package_id INNER JOIN Log_length ON Order_items.log_length_id = Log_length.log_length_id INNER JOIN Log_size ON Order_items.log_size_id = Log_size.log_size_id WHERE order_id = ?";
                  $orderItemsQuery = $db_handle->selectFrom($query, "i", $order["order_id"]);
                  $orderItemsQuery->bind_result($quantity, $package_type, $length, $image, $size);
                
                  $order_items = array();
                  while($orderItemsQuery->fetch()) {
                    $item["quantity"] = $quantity;
                    $item["package_type"] = $package_type;
                    $item["log_length"] = $length;
                    $item["image"] = $image;
                    $item["log_size"] = $size;
                    $order_items[] = $item;
                  }
                  $orderItemsQuery->free_result();
                  $orderItemsQuery->close();
              ?>
              <tbody class="table-tbody accordion-item sort-date" data-date="<?php echo strtotime($order["delivery_date"]); ?>">
                <tr class="separator" colspan="2"></tr>

                <tr class="accordion-header accordion-button collapsed rounded" data-bs-toggle="collapse" data-bs-target="#collapse-<?php echo $count; ?>" id="heading-<?php echo $count; ?>" aria-expanded="false">
                  <td><?php echo $order["order_id"]; ?></td>
                  <td><?php echo $city["name"]; ?></td>
                  <td><?php echo $street["name"]; ?></td>
                  <td><?php echo $order["phone"]; ?></td>
                  <td class="sort-date" data-date="<?php echo strtotime($order["delivery_date"]); ?>"><?php if(!is_null($order["delivery_date"])) echo date('m/Y',strtotime($order["delivery_date"])); ?></td>
                </tr>
                <tr class="border-style-none">
                  <td colspan="6" class="sort-date border-style-none p-0" data-date="<?php echo strtotime($order["delivery_date"]); ?>" >
                    <div class="accordion-collapse collapse" id="collapse-<?php echo $count; ?>" data-bs-parent="#accordion-example">
                      <div class="accordion-body card p-0">
                        <div calss="card-body">
                          <div class="card m-3">
                            <h3 class="card-header p-2">Stavke narudžbe</h3>
                            <div class="card-status-top bg-blue-brand"></div>
                            <div class="card-body">
                              <table class="table table-vcenter table-mobile-md border">
                                <thead>
                                  <tr>
                                    <th class="ps-4">Pakiranje</th>
                                    <th>Dužina</th>
                                    <th>Veličina</th>
                                    <th>Količina</th>
                                  </tr>

                                </thead>
                                <?php foreach ($order_items as $order_item): ?>
                                <tbody>

                                  <tr>
                                    <td class="ps-4"><?php echo $order_item["package_type"]; ?></td>
                                    <td><?php echo $order_item["log_length"]; ?></td>
                                    <td><?php echo $order_item["log_size"]; ?></td>
                                    <td><?php echo $order_item["quantity"]; ?></td>
                                  </tr>
                                  <?php endforeach; ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="card m-3">
                            <h3 class="card-header">Napomena</h3>
                            <div class="card-status-top bg-blue-brand"></div>
                            <div class="card-body">
                              <p class="text-muted">
                              <?php echo $order["note"]; ?>
                              </p>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>

                  </td>
                </tr>
              </tbody>

              <?php
              
                endforeach;
              endif;
              ?>
          </table>
        </div>

      </div>

    </div>
  </div>
</div>
<?php

include("footer.inc");
?>
