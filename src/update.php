<?php
    include('session.php');
    require_once("dbcontroller.php");

    $db_handle = new DBController();

    $package = $db_handle->runQuery("SELECT * FROM Package ORDER BY package_id ASC");
    $log_length = $db_handle->runQuery("SELECT * FROM Log_length ORDER BY log_length_id ASC");
    $log_size = $db_handle->runQuery("SELECT * FROM Log_size ORDER BY log_size_id ASC");

    $query = "SELECT order_id, phone, note, delivery_date FROM Orders WHERE order_id = ?";
    $queryOrderId = $db_handle->selectFrom($query, "i", $_GET["order_id"]);
    $queryOrderId->bind_result($orderId,$phone,$note,$deliveryDate);
    while ($queryOrderId->fetch()) {
        $order["order_id"] = $orderId;
        $order["phone"] = $phone;
        $order["note"] = $note;
        $order["delivery_date"] = $deliveryDate;
    }
    $queryOrderId->free_result();
    $queryOrderId->close();

    $query = "SELECT item_id, quantity, Package.type, Log_length.length, Log_length.image, Log_size.size FROM Order_items RIGHT JOIN Package ON Order_items.package_id = Package.package_id RIGHT JOIN Log_length ON Order_items.log_length_id = Log_length.log_length_id RIGHT JOIN Log_size ON Order_items.log_size_id = Log_size.log_size_id WHERE order_id = ? ORDER BY item_id";
    $orderItemsQuery = $db_handle->selectFrom($query, "i", $_GET["order_id"]);
    $orderItemsQuery->bind_result($id, $quantity, $packageType, $length, $logLengthImage, $size);

    $query = "SELECT Street.name FROM Orders INNER JOIN Street ON Orders.street_id = Street.street_id WHERE order_id = ?";
    $queryStreet = $db_handle->selectFrom($query, "i", $order["order_id"]);
    $queryStreet->bind_result($street_name);
  
    while($queryStreet->fetch()) {
      $street["name"] = $street_name;
    }
    $queryStreet->free_result();
    $queryStreet->close();
  
    $query = "SELECT City.name FROM Orders INNER JOIN City ON Orders.city_id = City.city_id WHERE order_id = ?";
    $queryCity = $db_handle->selectFrom($query, "i", $order["order_id"]);
    $queryCity->bind_result($city_name);
  
    while($queryCity->fetch()) {
      $city["name"] = $city_name;
    }
    $queryCity->free_result();
    $queryCity->close();

    $order_items = array();

    while($orderItemsQuery->fetch()) {
      $item["id"] = $id;
      $item["quantity"] = $quantity;
      $item["package_type"] = $packageType;
      $item["log_length"] = $length;
      $item["image"] = $logLengthImage;
      $item["log_size"] = $size;
      $order_items[] = $item;
    }

    $orderItemsQuery->free_result();
    $orderItemsQuery->close();

    //$update_order_status = $db_handle->insertInto("UPDATE Order_items SET quantity = ?, item_id = ? WHERE order_id = ?");
    //$update_order_status->bind_param('i', $_POST['confirm']);
    //$update_order_status->execute();

    include("header.inc");
?>
<div class="container-xl">
  <div class="page-header d-print-none">
    <div class="row g-2 align-items-center">
      <div class="col">
        <h2 class="page-title">
          <span>Pregled narudžbi</span>
        </h2>
      </div>
    </div>
  </div>
</div>
<div class="page-body">
  <div class="container-xl">
        <div>
                <form method="post" class="m-0" action="edit.php">                  
                      <div class="card px-0 pb-0">
                        <div class="card-body">
                          <!-- <div class="row"> -->
                          <div class="card m-3">
                                <h3 class="card-header">Podaci o kupcu</h3>
                                <div class="card-status-top bg-blue-brand"></div>
                                <div class="card-body">
                                  <div class="table-responsive-sm">
                                    <table class="table table-vcenter border">
                                      <thead>
                                        <tr>
                                          <th>Narudžba br.</th>
                                          <th>Grad</th>
                                          <th>Ulica</th>
                                          <th>Broj telefona</th>
                                          <th>Isporuka</th>
                                        </tr>
                                      </thead>
                                      <tbody class="table-tbody">
                                        <tr>
                                          <td><?php echo $order["order_id"]; ?></td>
                                          <td><?php echo $city["name"]; ?></td>
                                          <td><?php echo $street["name"]; ?></td>
                                          <td><?php echo $order["phone"]; ?></td>
                                          <td><?php if(!is_null($order["delivery_date"])) echo date('m/Y',strtotime($order["delivery_date"])); ?></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            <div class="card m-3">
                              <h3 class="card-header">Stavke narudžbe</h3>
                              <div class="card-status-top bg-blue-brand"></div>
                              <div class="card-body">
                                <table class="table table-vcenter border">
                                  <thead>
                                    <tr>
                                      <th class="w-0"></th>
                                      <th class="ps-4">Pakiranje</th>
                                      <th>Dužina</th>
                                      <th>Veličina</th>
                                      <th>Količina</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <input type="hidden" name="order_id" value="<?php echo $_GET["order_id"] ?>" />
                                      <?php foreach ($order_items as $order_item): ?>
                                      <tr>
                                        <td>
                                          <label class="form-check">
                                          <input type="hidden" name="item_id[]" value="<?php echo $order_item["id"]?>" />
                                          <input name="items[]" class="form-check-input" type="checkbox" value="<?php echo $order_item["id"];?>" />                          
                                        </label>  
                                      </td>
                                      <td class="w-4">
                                          <select name="package-id[]" class="form-select">
                                              <?php foreach ($package as $key=>$value):
                                                if($package[$key]["type"] != $order_item["package_type"]): ?>
                                                        <option value="<?php echo $package[$key]["package_id"] ?>"><?php echo $package[$key]["type"]; ?></option>
                                                      <?php else:  ?>
                                                        <option value="<?php echo $package[$key]["package_id"] ?>" selected><?php echo $order_item["package_type"] ?></option>
                                                  <?php endif; ?>
                                              <?php endforeach; ?>
                                          </select>
                                      </td>
                                      <td class="w-1">
                                          <select name="log-length-id[]" class="form-select">
                                            <?php foreach ($log_length as $key=>$value):
                                                      if($log_length[$key]["length"] != $order_item["log_length"]): ?>
                                                          <option value="<?php echo $log_length[$key]["log_length_id"] ?>"><?php echo $log_length[$key]["length"]; ?></option>
                                                      <?php else:  ?>
                                                      <option value="<?php echo $log_length[$key]["log_length_id"] ?>" selected><?php echo $order_item["log_length"] ?></option>
                                                <?php endif; ?>
                                              <?php endforeach; ?>
                                          </select>
                                      </td>
                                      <td class="w-4">
                                          <select name="log-size-id[]" class="form-select">
                                              <?php foreach ($log_size as $key=>$value):
                                                      if($log_size[$key]["size"] != $order_item["log_size"]): ?>
                                                        <option value="<?php echo $log_size[$key]["size"] ?>"><?php echo $log_size[$key]["size"]; ?></option>
                                                      <?php else:  ?>
                                                        <option value="<?php echo $log_size[$key]["log_size_id"] ?>" selected><?php echo $order_item["log_size"] ?></option>
                                                  <?php endif; ?>
                                              <?php endforeach; ?>
                                          </select>
                                      </td>
                                      <td class="w-1">
                                          <input name="quantity[]" type="number" class="form-control" value="<?php echo $order_item["quantity"]; ?>">
                                      </td>
                                    </tr>
                                    <?php endforeach; ?>
                                  </tbody>
                                </table>
                                </div>
                                <div class="card-footer d-inline-flex justify-content-between">
                            <button type="submit" name="add" class="btn" value="<?php echo $order["order_id"]; ?>">Dodajte proizvod</button>
                            <button type="submit" name="delete" class="btn btn-outline-danger" value="<?php echo $order["order_id"]; ?>">Obrišite odabrane stavke</button>
                          </div>
                              </div>
                            <!-- </div> -->
                            <!-- <div class="row mt-3"> -->
                              <div class="card m-3">
                                <h3 class="card-header">Napomena</h3>
                                <div class="card-status-top bg-blue-brand"></div>
                                <div class="card-body">
                                  <p class="text-muted">
                                      <input name="note" type="text" class="form-control" value="<?php echo $order["note"]; ?>">
                                  </p>
                                  <div class="input-icon col-sm-12 col-md-6 col-lg-6 mt-3">
                                      <label class="form-label">Isporuka(Mjesec)</label>
                                      <input id="datepicker-icon" class="form-control" type="month" name="delivery-date" value="<?php if(!is_null($order["delivery_date"])) echo date('Y-m',strtotime($order["delivery_date"])); ?>">
                                  </div>
                                </div>
                              </div>
                            <!-- </div>     -->

                          </div>
                          <div class="card-footer d-inline-flex justify-content-between">                          
                            <button type="submit" name="update" class="btn btn-secondary" value="<?php echo $order["order_id"]; ?>">Ažurirajte narudžbu ></button>
                          </div>
                        </div>
                </form>
    </div>
  </div>
</div>
<?php include("footer.inc");?>