<?php
include('session.php');

require_once("dbcontroller.php");
$db_handle = new DBController();


if (!empty($_POST["street"]) && !empty($_POST["city"]) && !empty($_POST["phone"]) && !empty($_SESSION["cart_item"])) {
    $query = "INSERT INTO Street(name) SELECT * FROM (SELECT NULLIF(?,'') AS name) AS temp WHERE NOT EXISTS (SELECT name FROM Street WHERE name IN (?)) LIMIT 1";
    $insertStreet = $db_handle->selectFrom($query, "ss", $_POST["street"], $_POST["street"]);
    $insertStreet->close();

    $query = "INSERT INTO City(name) SELECT * FROM (SELECT NULLIF(?,'') AS name) AS temp WHERE NOT EXISTS (SELECT name FROM City WHERE name IN (?)) LIMIT 1";
    $insertCity = $db_handle->selectFrom($query, "ss", $_POST["city"], $_POST["city"]);
    $insertCity->close();

    $query = "SELECT street_id FROM Street WHERE name = ?";
    $queryStreetId = $db_handle->selectFrom($query, "s", $_POST["street"]);
    $queryStreetId->bind_result($streetId);
    while ($queryStreetId->fetch()) {
        $street["id"] = $streetId;
    }
    $queryStreetId->free_result();
    $queryStreetId->close();

    $query = "SELECT city_id FROM City WHERE name = ?";
    $queryCityId = $db_handle->selectFrom($query, "s", $_POST["city"]);
    $queryCityId->bind_result($cityId);
    while ($queryCityId->fetch()) {
        $city["id"] = $cityId;
    }
    $queryCityId->free_result();
    $queryCityId->close();

    $query = "INSERT INTO Orders(street_id, city_id, phone, note, order_date, delivery_date) SELECT * FROM (SELECT NULLIF(?,'') AS street_id, NULLIF(?,'') AS city_id, NULLIF(?,'') AS phone, NULLIF(?,'') AS note, now() AS order_date, NULLIF(?,'') AS delivery_date) AS temp";
    $insertOrder = $db_handle->selectFrom($query, "iisss", $street["id"], $city["id"], $_POST["phone"], $_POST["note"], date('Y-m-d',strtotime($_POST["delivery-date"])));
    $db_handle->setInsertId();
    $insertOrder->close();

    foreach ($_SESSION["cart_item"] as $item) {
        $query = "INSERT INTO Order_items(order_id, item_id, package_id, log_length_id, log_size_id, quantity) VALUES (NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,''), NULLIF(?,''))";
        $insertOrderItems = $db_handle->selectFrom($query, "iiiiii", $db_handle->getInsertId(), $item["item_no"], $item["package_id"], $item["log_length_id"], $item["log_size_id"], $item["quantity"]);
        $insertOrderItems->close();
    }
    header("Refresh:0;" . "index.php", true, 303);
    unset($_SESSION["cart_item"]);
} else {
    header("Refresh:0;" . "order.php", true, 303);
}
?>