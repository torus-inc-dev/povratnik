CREATE DATABASE povratnik;

USE povratnik;

CREATE TABLE Package (
  package_id INT NOT NULL,
  type VARCHAR(20) NOT NULL,
  image TEXT NOT NULL,
  PRIMARY KEY (package_id)
);

CREATE TABLE Log_length (
  log_length_id INT NOT NULL,
  length VARCHAR(7) NOT NULL,
  image TEXT NOT NULL,
  PRIMARY KEY (log_length_id)
);

CREATE TABLE Log_size (
  log_size_id INT NOT NULL,
  size VARCHAR(20) NOT NULL,
  PRIMARY KEY (log_size_id)
);

CREATE TABLE Street (
  street_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  PRIMARY KEY (street_id),
  UNIQUE (name)
);

CREATE TABLE City (
  city_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (city_id),
  UNIQUE (name)
);

CREATE TABLE Order_status (
  order_status_id INT NOT NULL,
  order_status_description VARCHAR(30) NOT NULL,
  PRIMARY KEY (order_status_id)
);

CREATE TABLE Orders (
  order_id INT NOT NULL AUTO_INCREMENT,
  street_id INT NOT NULL,
  city_id INT NOT NULL,
  phone VARCHAR(20) NOT NULL,
  order_status_id INT NOT NULL DEFAULT '0',
  note VARCHAR(5000),
  order_date DATE NOT NULL,
  delivery_date DATE,
  PRIMARY KEY (order_id),
  FOREIGN KEY (street_id) REFERENCES Street(street_id),
  FOREIGN KEY (city_id) REFERENCES City(city_id),
  FOREIGN KEY (order_status_id) REFERENCES Order_status(order_status_id)
);

CREATE TABLE Order_items (
  order_id INT NOT NULL,
  item_no INT NOT NULL,
  package_id INT NOT NULL,
  log_length_id INT NOT NULL,
  log_size_id INT NOT NULL,
  quantity INT NOT NULL,
  UNIQUE (order_id, item_no),
  FOREIGN KEY (order_id) REFERENCES Orders(order_id) ON DELETE CASCADE,
  FOREIGN KEY (package_id) REFERENCES Package(package_id),
  FOREIGN KEY (log_length_id) REFERENCES Log_length(log_length_id),
  FOREIGN KEY (log_size_id) REFERENCES Log_size(log_size_id)
);

INSERT INTO `Package` (`package_id`, `type`) VALUES ('1','Paleta','product-images/palette.jpg'), ('2', 'Vreća','product-images/sack.png');
INSERT INTO `Log_length` (`log_length_id`, `length`, `image`) VALUES ('1', '25 cm', 'product-images/25cm.jpg'), ('2', '33 cm', 'product-images/33cm.jpg');
INSERT INTO `Log_size` (`log_size_id`, `size`) VALUES ('1', 'Sitno'), ('2', 'Srednje'), ('3', 'Krupno');
INSERT INTO `Order_status` (`order_status_id`, `order_status_description`) VALUES ('0','U obradi'), ('1', 'Poslano');
